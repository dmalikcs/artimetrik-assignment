import django.views.static
import django.views.generic
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings




from rest_framework import routers
from weblog.views import ArticleViewSet, \
    ArticleCreateViewSet, ArticleVoteUpViewSet

from altimetrik.views import IndexRedirectView

class SettingsTemplateView(django.views.generic.TemplateView):
    def get_context_data(self, **kwargs):
        context = super(SettingsTemplateView, self).get_context_data(**kwargs)
        context['settings'] = settings
        return context
#

urlpatterns = [
    url('^$', IndexRedirectView.as_view()),
    url('^robots.txt$', SettingsTemplateView.as_view(
        template_name='robots.txt', content_type='text/plain'
    )),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^weblog/articles/$', ArticleViewSet.as_view(), name='article-list'),
    url(r'^weblog/article/add/$', ArticleCreateViewSet.as_view(), name='article-add'),
    url(r'^weblog/article/(?P<pk>\d+)/up/$', ArticleVoteUpViewSet.as_view(), name='article-vote-up'),
    url(r'^weblog/article/(?P<pk>\d+)/$', ArticleViewSet.as_view(), name='article-detail'),

]

#
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', django.views.static.serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]