from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import RedirectView
from django.urls import reverse_lazy

def home(request):
    template_name = 'index.html'
    return render_to_response(
        template_name,
        {},
        RequestContext(request)
    )


class IndexRedirectView(RedirectView):
    url = reverse_lazy('article-add')


