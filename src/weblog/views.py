__author__ = 'dmalik'

from rest_framework.generics import  GenericAPIView, ListAPIView, \
    CreateAPIView, RetrieveAPIView

from rest_framework.response import Response

from .models import Article
from .serializers import ArticleSerializer, \
    ArticleCreateSerializer

class ArticleViewSet(ListAPIView):

    def get_queryset(self):
        return Article.objects.all().order_by('-vote')

    def list(self, request, pk=None):
        query = self.get_queryset()
        serializer = ArticleSerializer(query, many=True, context={'request': request})
        return Response(serializer.data)


class ArticleCreateViewSet(CreateAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleCreateSerializer


class ArticleVoteUpViewSet(RetrieveAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleCreateSerializer

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.vote_up()
        return super(ArticleVoteUpViewSet, self).get(request, *args, **kwargs)