__author__ = 'dmalik'

from .models import Article
from rest_framework import serializers

class ArticleSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Article
        fields = ('title', 'content', 'author', 'vote', 'url')



class ArticleCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = ('title', 'content', 'author')
