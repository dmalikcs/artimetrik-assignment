# Generated by Django 2.0 on 2018-08-03 09:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('weblog', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='author',
        ),
        migrations.AddField(
            model_name='article',
            name='author',
            field=models.CharField(default='Deepak Malik', max_length=35, verbose_name='Author name'),
            preserve_default=False,
        ),
    ]
