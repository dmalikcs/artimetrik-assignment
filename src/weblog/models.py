__author__ = 'dmalik'

from django.db import models

# class Author(models.Model):
#     name = models.CharField(max_length=75)
#     email = models.EmailField(null=True, blank=True)
#     ### we can use django-extra packages to get following field by default
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#
#     def __str__(self):
#         return self.name
#

class Article(models.Model):
    author = models.CharField('Author name', max_length=35)
    title = models.CharField(max_length=75)
    content = models.TextField()
    vote = models.PositiveIntegerField(default=0)

    ### we can use django-extra packages to get following field by default
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.content

    def vote_up(self):
        self.vote += 1
        self.save()

    def vote_down(self):
        self.vote -= 1
        self.vote()